import React from 'react'
import './header.scss'
import { Navbar } from '../navbar/Navbar';

export const Header = () =>{

    return(
        <div className = 'header'>
            <div className = "headerWrap">
            
                <div className = "logo">
                <a href = "/">
                    <h2>Конвертер валют</h2>
                </a>
                </div>

               <Navbar/>
            
            </div>

            <hr/>
        
        </div>
    )
}
