import React from 'react'
import {NavLink} from 'react-router-dom'
import './navbar.scss'


export const Navbar = () =>{

    return(

        <nav>
            <ul>
                <li><NavLink to = '/'>Главная</NavLink></li>
                <li><NavLink to = '/calc'>Калькулятор</NavLink></li>
            </ul>
        </nav>
    )
}
