import React from 'react';

import './App.scss';
import Layout from './components/layout/Layout';
import {RateContext} from './context/RateContext'

import CHF from './image/CHF.png';
import CNY from './image/CNY.png';
import EUR from './image/EUR.png';
import GBP from './image/GBP.png';
import JPY from './image/JPY.png';
import RUB from './image/RUB.png';
import USD from './image/USD.png';
import UAH from './image/UAH.png';
import {Exchange} from "./components/exchange/Exchange";
import {Counter} from "./components/counter/Counter";
import {CountResult} from "./components/countResult/CountResult";



class App extends React.Component{
  constructor(props){
    super(props);

    this.state = {

      base: 'UAH',
      rate: '',
      date: '',
                  currency: { UAH: {name: 'Гривна', flag: UAH, course: ''},
                  USD: {name: 'Доллар США', flag: USD, course: ''},
                  EUR: {name: 'Евро', flag: EUR, course: ''},

      },

      //calculator

      inputValue: 100,
      currencyValue: 'UAH',
      result: null,



    }




  }


  inputValueHandler = (event) =>{
      this.setState({inputValue: event.target.value, 
                      result: null
      })

  }

  currencyValueHandler = (event) =>{
    this.setState({currencyValue: event.target.value,
                  result: null  
    })
  }


  calculatorHandler = async (value) =>{

    let result

      var myHeaders = new Headers();
      myHeaders.append("apikey", "NPvv0tKEqPYRGNpm9VyvAqnG2gBZ5PVS");

      var requestOptions = {
          method: 'GET',
          redirect: 'follow',
          headers: myHeaders
      };

    await fetch(`https://api.apilayer.com/exchangerates_data/latest?base=UAH`, requestOptions)
      .then((response)=> response.json()).then((response)=>{



            console.log(response)
        
        result = response.rates[value] * this.state.inputValue

      })

      this.setState({result})

  }


  
  componentDidMount(){
      var myHeaders = new Headers();
      myHeaders.append("apikey", "NPvv0tKEqPYRGNpm9VyvAqnG2gBZ5PVS");

      var requestOptions = {
          method: 'GET',
          redirect: 'follow',
          headers: myHeaders
      };

    fetch(`https://api.apilayer.com/exchangerates_data/latest?base=${this.state.base}`, requestOptions)
    .then((response)=> response.json()).then((response)=>{
      console.log(response)
      const rateArr = ['UAH','USD','EUR'];
      const currency = {...this.state.currency};

      for(let i = 0; i < rateArr.length; i++){
        currency[rateArr[i]].course = response.rates[rateArr[i]]
      }

      this.setState({
            rate: response.rate,
            date: response.date,
            currency

      })
      



    })


  }




  render(){

    return(
      <RateContext.Provider 
        value = {{state: this.state, 
                  inputValueHandler: this.inputValueHandler,
                  currencyValueHandler: this.currencyValueHandler,
                  calculatorHandler: this.calculatorHandler
                  }}>
          <Exchange/>

          <Counter/>
          <CountResult/>
      </RateContext.Provider>
      
    )
  }
}


export default App
